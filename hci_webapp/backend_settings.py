# Settings for backend host

HOSTNAME = 'https://hci.roland.tardieu.net/'


# Settings from backend API

TOKEN_OBTAIN = HOSTNAME + "api/token/"
TOKEN_REFRESH = HOSTNAME + "api/token/refresh/"

HEATER_VALVES = HOSTNAME + "api/heatervalves/"
ACTION_UPDATE_VALVE_TEMPERATURE = "update_temperature/"
ACTION_UPDATE_VALVE_SCHEDULE = "update_schedule/"

TEMPERATURE_SCHEDULES = HOSTNAME + "api/temperatureschedules/"
ACTION_UPDATE_SCHEDULE_NAME = "update_name/"
ACTION_UPDATE_SCHEDULE_SCHEDULE = "update_schedule/"

DEFAULT_TEMPERATURES = HOSTNAME + "api/defaulttemperatures/"
ACTION_UPDATE_DEFAULT_TEMPERATURE_TEMPERATURE = "update_temperature/"