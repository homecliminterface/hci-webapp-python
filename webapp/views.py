from django.shortcuts import render, redirect, HttpResponse
from django.views import View
from django.contrib import messages

import requests

from webapp.forms import LoginForm, CreateDefaultTemperatureForm, UpdateDefaultTemperatureForm, \
    UpdateHeaterValveTemperatureForm, UpdateHeaterValveScheduleForm, CreateNewScheduleForm, \
    UpdateTemperatureScheduleNameForm, UpdateTemperatureScheduleDayScheduleForm, DayChoicesForm
from webapp.helpers import api_response_and_refresh_token, Choices, ScheduleTableFromSchedule, hour_minute_count_down, \
    day_hour_minute_count_down
from hci_webapp.backend_settings import TOKEN_OBTAIN, HEATER_VALVES, ACTION_UPDATE_VALVE_TEMPERATURE, \
    TEMPERATURE_SCHEDULES, ACTION_UPDATE_SCHEDULE_NAME, DEFAULT_TEMPERATURES, \
    ACTION_UPDATE_DEFAULT_TEMPERATURE_TEMPERATURE, ACTION_UPDATE_VALVE_SCHEDULE, ACTION_UPDATE_SCHEDULE_SCHEDULE
from hci_webapp.settings import JWT_HTTP_ONLY


class LogoutView(View):
    template_name = 'webapp/logout.html'

    def get(self, request):
        return render(request, self.template_name, context={'username': request.COOKIES['username']})

    def post(self, request):
        response = redirect('/')
        response.delete_cookie('access')
        response.delete_cookie('refresh')
        response.delete_cookie('username')
        return response


class LoginView(View):
    template_name = 'webapp/login.html'

    def get(self, request):
        if 'access' in request.COOKIES:
            return redirect('logout')
        form = LoginForm()
        return render(request, self.template_name, context={'form': form, 'message': 'Please sign in.'})

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            api_response = requests.post(
                f'{TOKEN_OBTAIN}',
                json={"username": username, "password": password}
            )
            if api_response.status_code == 401:
                return render(
                    request,
                    self.template_name,
                    context={'form': form, 'message': 'Bad username or password.'}
                )
            if api_response.status_code == 200:
                refresh = api_response.json()["refresh"]
                access = api_response.json()["access"]
                response = redirect('/')
                response.set_cookie('access', access, httponly=JWT_HTTP_ONLY)
                response.set_cookie('refresh', refresh, httponly=JWT_HTTP_ONLY)
                response.set_cookie('username', username, httponly=JWT_HTTP_ONLY)
                return response
            else:
                return HttpResponse("<h1>Erreur "+str(api_response.status_code)+"</h1>")


class HeaterValvesView(View):

    template_name = 'webapp/heater_valves.html'

    def get(self, request):
        if 'access' not in request.COOKIES:
            return redirect('login')
        api_response, access_token = api_response_and_refresh_token(requests.get, HEATER_VALVES, request)
        if api_response.status_code != 200:
            return HttpResponse(f'<h1>Error {api_response.status_code}<h1>')
        valves = api_response.json()
        for valve in valves:
            valve['id'] = str(valve['id'])
        update_wanted_temperature_form = UpdateHeaterValveTemperatureForm
        update_schedule_form = UpdateHeaterValveScheduleForm
        update_schedule_form.base_fields['schedule'].choices, access_token = Choices.from_api_object_and_refresh_token(
            request,
            TEMPERATURE_SCHEDULES,
            token=access_token
        )
        context = {
            "valves": valves,
            "number_of_valves": len(valves),
            "update_wanted_temperature_form": update_wanted_temperature_form,
            "update_schedule_form": update_schedule_form,
        }
        response = render(request, self.template_name, context=context)
        if access_token:
            response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
        return response

    def post(self, request):
        access_token = None
        if "unset_schedule" in request.POST:
            valve_id = request.POST["unset_schedule"]
            api_response, access_token = api_response_and_refresh_token(
                requests.put,
                HEATER_VALVES + f"{valve_id}/" + ACTION_UPDATE_VALVE_SCHEDULE,
                request,
                json={"schedule": None}
            )
            if api_response.status_code != 200:
                return HttpResponse(api_response)
            messages.info(
                request,
                "Temperature schedule unset for this valve",
                extra_tags=f'{valve_id}'
            )
        if "update_schedule" in request.POST:
            update_schedule_form = UpdateHeaterValveScheduleForm(request.POST)
            if update_schedule_form.is_valid():
                valve_id = request.POST["update_schedule"]
                schedule = update_schedule_form.cleaned_data["schedule"]
                api_response, access_token = api_response_and_refresh_token(
                    requests.put,
                    HEATER_VALVES + f"{valve_id}/" + ACTION_UPDATE_VALVE_SCHEDULE,
                    request,
                    json={"schedule": schedule}
                )
                if api_response.status_code != 200:
                    return HttpResponse(f'<h1>Error {api_response.status_code}<h1>')
                messages.info(
                    request,
                    "Temperature schedule updated",
                    extra_tags=f'{valve_id}'
                )
        if "update_temperature" in request.POST:
            update_temperature_form = UpdateHeaterValveTemperatureForm(request.POST)
            if update_temperature_form.is_valid():
                valve_id = request.POST["update_temperature"]
                wanted_temperature = update_temperature_form.cleaned_data["wanted_temperature"]
                api_response, access_token = api_response_and_refresh_token(
                    requests.put,
                    HEATER_VALVES + f"{valve_id}/" + ACTION_UPDATE_VALVE_TEMPERATURE,
                    request,
                    json={"wanted_temperature": wanted_temperature}
                )
                if api_response.status_code != 200:
                    return HttpResponse(f'<h1>Error {api_response.status_code}<h1>')
                messages.info(
                    request,
                    f'Temperature set to {wanted_temperature}°C. Refresh page to update displayed temperature.',
                    extra_tags=f'{valve_id}'
                )
        response = redirect('heater_valves')
        if access_token:
            response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
        return response


class TemperatureSchedulesView(View):

    template_name = 'webapp/schedules.html'

    def get(self, request):
        if 'access' not in request.COOKIES:
            return redirect('login')
        api_response, access_token = api_response_and_refresh_token(requests.get, TEMPERATURE_SCHEDULES, request)
        if api_response.status_code != 200:
            return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
        schedules = api_response.json()
        create_form = CreateNewScheduleForm
        context = {"schedules": schedules, "create_form": create_form}
        response = render(request, self.template_name, context=context)
        if access_token:
            response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
        return response

    def post(self, request):
        access_token = None
        if "create_schedule" in request.POST:
            create_form = CreateNewScheduleForm(request.POST)
            if create_form.is_valid():
                schedule_name = create_form.cleaned_data['name']
                api_response, access_token = api_response_and_refresh_token(
                    requests.post,
                    TEMPERATURE_SCHEDULES,
                    request,
                    json={"name": schedule_name}
                )
                if api_response.status_code == 400:
                    messages.info(
                        request,
                        "Schedule not created, because name already exists in database."
                    )
                    response = redirect('temperature_schedules')
                    if access_token:
                        response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
                    return response
                if api_response.status_code != 201:
                    return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
                schedule_id = api_response.json()["id"]
                api_response, access_token = api_response_and_refresh_token(
                    requests.get,
                    TEMPERATURE_SCHEDULES + f"{schedule_id}/",
                    request,
                    token=access_token
                )
                if api_response.status_code != 200:
                    return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
                response = redirect('temperature_schedule_detail', schedule_id)
                if access_token:
                    response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
                return response
            return redirect('temperature_schedules')
        if "delete_schedule" in request.POST:
            schedule_id = request.POST['delete_schedule']
            api_response, access_token = api_response_and_refresh_token(
                requests.delete,
                TEMPERATURE_SCHEDULES + f"{schedule_id}/",
                request
            )
            if api_response.status_code == 500:
                messages.info(
                    request,
                    "Impossible to delete schedule because it is currently used by one or more heater valve(s)."
                )
            elif api_response.status_code != 204:
                return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
            else:
                messages.info(
                    request,
                    "Temperature schedule deleted."
                )
            response = redirect('temperature_schedules')
            if access_token:
                response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
            return response


class TemperatureScheduleDetailView(View):

    template_name = 'webapp/schedule_detail.html'

    def get(self, request, schedule_id):
        if 'access' not in request.COOKIES:
            return redirect('login')
        api_response, access_token = api_response_and_refresh_token(
            requests.get,
            TEMPERATURE_SCHEDULES + f"{schedule_id}/",
            request
        )
        if api_response.status_code != 200:
            return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
        json_schedule = api_response.json()
        schedule_for_table, legend = \
            ScheduleTableFromSchedule(json_schedule["schedule"]).get_table_dict_and_color_classes()
        api_response, access_token = api_response_and_refresh_token(
            requests.get,
            DEFAULT_TEMPERATURES,
            request,
            token=access_token
        )
        if api_response.status_code != 200:
            return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
        default_temperatures = api_response.json()
        preset_temperatures = dict()
        for default_temperature in default_temperatures:
            preset_temperatures[default_temperature["letter_code"]] = \
                {"name": default_temperature["name"], "temperature": default_temperature["temperature"]}
        update_name_form = UpdateTemperatureScheduleNameForm
        update_day_schedule_form = UpdateTemperatureScheduleDayScheduleForm
        copy_day_schedule_form = DayChoicesForm
        context = {
            "json_schedule": json_schedule,
            "schedule_for_table": schedule_for_table,
            "legend": legend,
            "preset_temperatures": preset_temperatures,
            "update_name_form": update_name_form,
            "update_day_schedule_form": update_day_schedule_form,
            "copy_day_schedule_form": copy_day_schedule_form
        }
        response = render(request, self.template_name, context=context)
        if access_token:
            response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
        return response

    def post(self, request, schedule_id):
        access_token = None
        if "update_name" in request.POST:
            update_name_form = UpdateTemperatureScheduleNameForm(request.POST)
            if update_name_form.is_valid():
                name = update_name_form.cleaned_data["name"]
                api_response, access_token = api_response_and_refresh_token(
                    requests.put,
                    TEMPERATURE_SCHEDULES + f"{schedule_id}/" + ACTION_UPDATE_SCHEDULE_NAME,
                    request,
                    json={"name": name}
                )
                if api_response.status_code == 400:
                    try:
                        if "already exists" in api_response.json()["name"][0]:
                            messages.info(
                                request,
                                'Name not modified (already exists in database)'
                            )
                    except Exception as e:
                        return HttpResponse(f'<h1>Error {api_response.status_code}</h1> <p>{e}</p>')
                elif api_response.status_code != 200:
                    return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
                else:
                    messages.info(
                        request,
                        f'Name modified',
                    )

        if "add_day_schedule_entry" in request.POST or "delete_day_schedule_entry" in request.POST \
                or "copy_day_schedule" in request.POST:
            day = request.POST["day"]
            api_response, access_token = api_response_and_refresh_token(
                requests.get,
                TEMPERATURE_SCHEDULES + f"{schedule_id}/",
                request
            )
            if api_response.status_code != 200:
                return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
            schedule = api_response.json()["schedule"]

            if "copy_day_schedule" in request.POST:
                copy_day_schedule_form = DayChoicesForm(request.POST)
                if copy_day_schedule_form.is_valid():
                    target_day = copy_day_schedule_form.cleaned_data["target_day"]
                    schedule[target_day] = schedule[day]

            if "delete_day_schedule_entry" in request.POST:
                begin_hour = int(request.POST["begin_hour"])
                begin_minute = int(request.POST["begin_minute"])
                schedule[day][str(begin_hour)].pop(str(begin_minute), 0)

            if "add_day_schedule_entry" in request.POST:
                update_day_schedule_form = UpdateTemperatureScheduleDayScheduleForm(request.POST)
                if update_day_schedule_form.is_valid():
                    start_hour, start_minute = Choices.get_hour_and_minute_from_time_choice(
                        int(update_day_schedule_form.cleaned_data["start"])
                    )
                    end_hour, end_minute = Choices.get_hour_and_minute_from_time_choice(
                        int(update_day_schedule_form.cleaned_data["end"])
                    )
                    temperature = update_day_schedule_form.cleaned_data["temperature"]
                    try:
                        temperature = float(temperature)
                    except ValueError:
                        pass
                    previous_temperature = 'h'  # À voir si on conserve cette valeur car elle peut être incohérente
                    for (count_day, hour, minute) in day_hour_minute_count_down(int(day), start_hour, start_minute):
                        if str(minute) in schedule[str(count_day)][str(hour)]:
                            previous_temperature = schedule[str(count_day)][str(hour)][str(minute)]
                            break
                    try:
                        previous_temperature = float(previous_temperature)
                    except ValueError:
                        pass
                    schedule[day][str(start_hour)][str(start_minute)] = temperature
                    if (end_hour, end_minute) == (24, 0):
                        day = str((int(day) + 1) % 6)
                        end_hour = 0
                        end_minute = 0
                    schedule[day][str(end_hour)][str(end_minute)] = previous_temperature
                    for (hour, minute) in hour_minute_count_down(end_hour, end_minute, start_hour, start_minute+1):
                        schedule[day][str(hour)].pop(str(minute), 0)

            api_response, access_token = api_response_and_refresh_token(
                requests.put,
                TEMPERATURE_SCHEDULES + f"{schedule_id}/" + ACTION_UPDATE_SCHEDULE_SCHEDULE,
                request,
                json={"schedule": schedule},
                token=access_token
            )
            if api_response.status_code != 200:
                return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')

        response = redirect('temperature_schedule_detail', schedule_id)
        if access_token:
            response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
        return response


class DefaultTemperaturesView(View):

    template_name = 'webapp/default_temperatures.html'

    def get(self, request):
        if 'access' not in request.COOKIES:
            return redirect('login')
        api_response, access_token = api_response_and_refresh_token(requests.get, DEFAULT_TEMPERATURES, request)
        if api_response.status_code != 200:
            return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
        temperatures = api_response.json()
        for temperature in temperatures:
            temperature["id"] = str(temperature["id"])
        create_form = CreateDefaultTemperatureForm
        update_form = UpdateDefaultTemperatureForm
        context = {"temperatures": temperatures, "create_form": create_form, "update_form": update_form}
        response = render(request, self.template_name, context=context)
        if access_token:
            response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
        return response

    def post(self, request):
        access_token = None
        if "update" in request.POST:
            update_form = UpdateDefaultTemperatureForm(request.POST)
            if update_form.is_valid():
                default_temperature_id = request.POST["update"]
                api_response, access_token = api_response_and_refresh_token(
                    requests.put,
                    DEFAULT_TEMPERATURES + f'{default_temperature_id}/' + ACTION_UPDATE_DEFAULT_TEMPERATURE_TEMPERATURE,
                    request,
                    json={"temperature": update_form.cleaned_data["temperature"]}
                )
                if api_response.status_code != 200:
                    return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
                messages.info(
                    request,
                    message="Temperature updated.",
                    extra_tags=f"{default_temperature_id}"
                )
            response = redirect('default_temperatures')
            if access_token:
                response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
            return response
        if "delete" in request.POST:
            default_temperature_id = request.POST["delete"]
            api_response, access_token = api_response_and_refresh_token(
                requests.delete,
                DEFAULT_TEMPERATURES + f'{default_temperature_id}',
                request
            )
            if api_response.status_code != 204:
                return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
            messages.info(
                request,
                message="Preset temperature deleted.",
                extra_tags="general_info"
            )
            response = redirect('default_temperatures')
            if access_token:
                response.set_cookie('access', access_token, httponly=JWT_HTTP_ONLY)
            return response
        if "create_default_temperature" in request.POST:
            create_form = CreateDefaultTemperatureForm(request.POST)
            if create_form.is_valid():
                api_response, access_token = api_response_and_refresh_token(
                    requests.post,
                    DEFAULT_TEMPERATURES,
                    request,
                    json={
                        "name": create_form.cleaned_data["name"],
                        "letter_code": create_form.cleaned_data["letter_code"],
                        "temperature": create_form.cleaned_data["temperature"]
                    }
                )
                if api_response.status_code == 400:
                    messages.info(
                        request,
                        message="Preset temperature was not created, because name or letter code was already existing.",
                        extra_tags="general_info"
                    )
                    return redirect('default_temperatures')
                if api_response.status_code != 201:
                    return HttpResponse(f'<h1>Error {api_response.status_code}</h1>')
                messages.info(
                    request,
                    message=f"{create_form.cleaned_data['name']} preset temperature created.",
                    extra_tags="general_info"
                )
            return redirect('default_temperatures')


