from django import template

register = template.Library()

@register.simple_tag(takes_context=True)
def preset_temperature_data(context, temperature, data):
    return context["preset_temperatures"][temperature][data]

@register.filter
def mindigits(value, number_of_digits):
    try:
        value = int(value)
    except ValueError:
        return value
    if value >= 10**number_of_digits:
        return value
    value = str(value)
    result = "0"*(number_of_digits-len(value)) + value
    return result

@register.filter
def percents_of_dayspace(value, percents=100):
    complete_day = 24 * 60
    number_of_percents = (value / complete_day) * percents
    return f"{number_of_percents}%"

