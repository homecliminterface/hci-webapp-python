from django.shortcuts import redirect, HttpResponse

import requests

from hci_webapp.backend_settings import TOKEN_REFRESH


def api_response_and_refresh_token(function, url, request, json=None, token=None):
    """Refresh access token if needed, and return API response from method, and refreshed token if exists."""
    if token is None:
        access_token = request.COOKIES['access']
    else:
        access_token = token
    headers = {'Authorization': f'Bearer {access_token}'}
    api_response = function(url, headers=headers, json=json)
    if api_response.status_code == 401:
        refresh_response = requests.post(TOKEN_REFRESH, json={"refresh": request.COOKIES['refresh']})
        if refresh_response.status_code != 200:
            return redirect('login'), None
        access_token = refresh_response.json()['access']
        headers = {'Authorization': f'Bearer {access_token}'}
        api_response = function(url, headers=headers, json=json)
        return api_response, access_token
    return api_response, None


def mindigits(value, number_of_digits):
    try:
        value = int(value)
    except ValueError:
        return value
    if value >= 10 ** number_of_digits:
        return value
    value = str(value)
    result = "0" * (number_of_digits - len(value)) + value
    return result


class Choices:

    @classmethod
    def from_json_list(cls, json_list, name='name'):
        choices = list()
        for instance in json_list:
            instance_name = instance['name']
            instance_id = instance['id']
            choices.append((instance_id, instance_name))
        return choices

    @classmethod
    def from_api_object_and_refresh_token(cls, request, object_list_endpoint, name='name', token=None):
        api_response, access_token = api_response_and_refresh_token(
            requests.get,
            object_list_endpoint,
            request,
            token=token
        )
        if api_response.status_code != 200:
            return HttpResponse(f'<h1>Error {api_response.status_code}')
        choices = cls.from_json_list(api_response.json(), name=name)
        return choices, access_token

    @classmethod
    def time_choices(cls, step=5):
        choices = list()
        for hour in range(0, 24):
            for minute in range(0, 60, step):
                choices.append(
                    (hour * 60 + minute,
                     mindigits(hour, 2) + ":" + mindigits(minute, 2)
                     )
                )
        choices.append((24 * 60, mindigits(24, 2) + ":" + mindigits(0, 2)))
        return choices

    @classmethod
    def get_hour_and_minute_from_time_choice(cls, time):
        hour = time // 60
        minute = time % 60

        return hour, minute


def get_duration(time1, time2):
    hour1, minute1 = time1
    hour2, minute2 = time2
    minutes1 = hour1 * 60 + minute1
    minutes2 = hour2 * 60 + minute2
    return max(minutes2 - minutes1, minutes1 - minutes2)


class ScheduleTableFromSchedule:
    DAYS_NAME = {
        0: "Monday",
        1: "Tuesday",
        2: "Wednesday",
        3: "Thursday",
        4: "Friday",
        5: "Saturday",
        6: "Sunday",
    }

    def __init__(self, schedule_dict):
        self.schedule_dict = schedule_dict
        self.color_classes = {
            "c": "comfort",
            "r": "reduced",
            "h": "holidays",
        }

    @classmethod
    def get_day_choices(cls):
        choices = list()
        for key, value in cls.DAYS_NAME.items():
            choices.append((key, value))
        return choices

    def reinit_color_classes(self):
        self.color_classes = {
            "c": "comfort",
            "r": "reduced",
            "h": "holidays",
        }

    def _get_color_class(self, temperature):
        if temperature in self.color_classes:
            return self.color_classes[temperature]
        else:
            i = 0
            while "c" + str(i) in self.color_classes.values():
                i += 1
            self.color_classes[temperature] = "c" + str(i)
            return "c" + str(i)

    def _generate_data(self):
        self.table_dict = dict()
        previous_temperature = None
        for (day, hour, minute) in day_hour_minute_count_down(6, 24, 0):
            if str(minute) in self.schedule_dict[str(day)][str(hour)]:
                previous_temperature = self.schedule_dict[str(day)][str(hour)][str(minute)]
                break;
        for day in range(7):
            str_day = str(day)
            if previous_temperature is None:
                previous_temperature = "unset"
            self.table_dict[day] = dict()
            day_table_dict = self.table_dict[day]
            day_table_dict["day_name"] = self.DAYS_NAME[day]
            day_table_dict["schedule"] = dict()
            day_schedule_table_dict = day_table_dict["schedule"]
            time = (0, 0)
            entry = 0
            for hour in range(24):
                str_hour = str(hour)
                for minute in range(60):
                    str_minute = str(minute)
                    if str_minute in self.schedule_dict[str_day][str_hour]:
                        precedent_time = time
                        time = (hour, minute)
                        precedent_duration = get_duration(precedent_time, time)
                        if precedent_time != time:
                            day_schedule_table_dict[entry] = {
                                "begin": {"hour": precedent_time[0], "minute": precedent_time[1]},
                                "duration": precedent_duration,
                                "temperature": previous_temperature,
                                "color_class": self._get_color_class(previous_temperature)
                            }
                            entry += 1
                        previous_temperature = self.schedule_dict[str_day][str_hour][str_minute]
            precedent_time = time
            time = (24, 0)
            precedent_duration = get_duration(precedent_time, time)
            day_schedule_table_dict[entry] = {
                "begin": {"hour": precedent_time[0], "minute": precedent_time[1]},
                "duration": precedent_duration,
                "temperature": previous_temperature,
                "color_class": self._get_color_class(previous_temperature)
            }
        print(self.table_dict)

    def get_table_dict(self):
        self._generate_data()
        return self.table_dict

    def get_table_dict_and_color_classes(self):
        table_dict = self.get_table_dict()
        color_classes = self.color_classes
        return table_dict, color_classes


def hour_minute_count_down(start_hour, start_minute, end_hour, end_minute):
    count = list()
    h = start_hour
    while h >= end_hour:
        if h == start_hour:
            min = start_minute - 1
        else:
            min = 59
        if h == end_hour:
            end_min = end_minute
        else:
            end_min = 0
        while min >= end_min:
            count.append((h, min))
            min -= 1
        h -= 1
    return count

def day_hour_minute_count_down(start_day, start_hour, start_minute):
    count = list()
    day = start_day
    while day >= 0:
        if day == start_day:
            start_h = start_hour
            start_m = start_minute
        else:
            start_h = 24
            start_m = 0
        day_count = hour_minute_count_down(start_h, start_m, 0, 0)
        for hour, minute in day_count:
            count.append((day, hour, minute))
        day -= 1
    return count
