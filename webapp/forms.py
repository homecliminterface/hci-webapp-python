from django import forms

from webapp.helpers import Choices, ScheduleTableFromSchedule


class LoginForm(forms.Form):
    username = forms.CharField(label="Username", max_length=50)
    password = forms.CharField(label="Password", max_length=50, widget=forms.widgets.PasswordInput())


class UpdateHeaterValveScheduleForm(forms.Form):
    schedule = forms.ChoiceField(label="")


class CreateDefaultTemperatureForm(forms.Form):
    name = forms.CharField(max_length=32)
    letter_code = forms.CharField(max_length=1)
    temperature = forms.FloatField()


class UpdateDefaultTemperatureForm(forms.Form):
    temperature = forms.FloatField(label="")


class UpdateHeaterValveTemperatureForm(forms.Form):
    wanted_temperature = forms.FloatField(label="")


class CreateNewScheduleForm(forms.Form):
    name = forms.CharField(max_length=64)


class UpdateTemperatureScheduleNameForm(forms.Form):
    name = forms.CharField(max_length=64)


class UpdateTemperatureScheduleDayScheduleForm(forms.Form):
    start = forms.ChoiceField(choices=Choices.time_choices())
    end = forms.ChoiceField(choices=Choices.time_choices())
    temperature = forms.CharField(max_length=2)


class DayChoicesForm(forms.Form):
    target_day = forms.ChoiceField(choices=ScheduleTableFromSchedule.get_day_choices(), label="")

